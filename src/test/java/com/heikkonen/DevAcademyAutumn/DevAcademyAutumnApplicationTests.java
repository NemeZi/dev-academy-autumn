package com.heikkonen.DevAcademyAutumn;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DevAcademyAutumnApplicationTests {

	@Autowired
	private VaccinesController controller;

	//Tests that order/vaccine amounts are received
	@Test
	void testAmount() { assertThat(this.controller.getTotalAmount()).as("Total amount").isNotNull();
	}
	//Checks that between 01.01.2021 01:01 and 03.03.2021 03:03 orders has arrived 2965 and vaccines 14838
	@Test
	void testTotal(){
		TotalResponse response = this.controller.getTotalOrderAndVaccinesAmount("2021-01-01T01:01", "2021-03-03T03:03");
		assertThat(response.vaccines).isEqualTo(14838);
		assertThat(response.orders).isEqualTo(2965);
	}
	//Checks that 12590 vaccines are expired before 12.04.2021 11:10
	@Test
	void testExpiredBeforeUsage(){
		VaccineExpiredBeforeUsage expired = this.controller.getTotalExpiredBeforeUsage("2021-04-12T11:10");
		assertThat(expired.expiredBeforeUsage).isEqualTo(12590);
	}
	//Makes sure 61 orders arrived on date 20.03.2021
	@Test
	void testArrivedByDate(){
		TotalResponse response = this.controller.getTotalOrderAndVaccinesAmount("2021-03-20T00:01", "2021-03-20T23:59");
		assertThat(response.orders).isEqualTo(61);
	}



}
