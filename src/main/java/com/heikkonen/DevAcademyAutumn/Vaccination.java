package com.heikkonen.DevAcademyAutumn;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Vaccination {
    
    @JsonProperty("vaccination-id")
    public String vaccinationId;
    public String sourceBottle;
    public String gender;
    public String vaccinationDate;

    public Vaccination(){}

    public Vaccination(String vaccinationId, String sourceBottle, String gender, String vaccinationDate){
        this.vaccinationId = vaccinationId;
        this.sourceBottle = sourceBottle;
        this.gender = gender;
        this.vaccinationDate = vaccinationDate;
    }

}
