package com.heikkonen.DevAcademyAutumn;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class VaccinesService {

    //Creates lists for all producers json and list for vaccination json
    List<Vaccine> solarBuddhica;
    List<Vaccine> zerpfy;
    List<Vaccine> antiqua;
    List<Vaccination> vaccinations;

    //Creates new Date and Time format for Date class
    DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm");
    Date sinceDate;
    Date untilDate;
    Date arrivedDate;
    Date dateVaccinated;
    Date tempDate;

    //Parses json files to java to be stored
    ObjectMapper mapper = new ObjectMapper();
    TypeReference<List<Vaccine>> typeReference = new TypeReference<List<Vaccine>>() {
    };
    TypeReference<List<Vaccination>> typeReference2 = new TypeReference<List<Vaccination>>() {
    };
    InputStream inputStream = TypeReference.class.getResourceAsStream("/json/SolarBuddhica.json");
    InputStream inputStream2 = TypeReference.class.getResourceAsStream("/json/Zerpfy.json");
    InputStream inputStream3 = TypeReference.class.getResourceAsStream("/json/Antiqua.json");
    InputStream inputStream4 = TypeReference.class.getResourceAsStream("/json/vaccinations.json");

    //Adds producers and vaccinations json to the list
    public VaccinesService() {

        try {
            List<Vaccine> solarBuddhica = mapper.readValue(inputStream, typeReference);
            this.solarBuddhica = solarBuddhica;
            List<Vaccine> zerpfy = mapper.readValue(inputStream2, typeReference);
            this.zerpfy = zerpfy;
            List<Vaccine> antiqua = mapper.readValue(inputStream3, typeReference);
            this.antiqua = antiqua;
            List<Vaccination> vaccinations = mapper.readValue(inputStream4, typeReference2);
            this.vaccinations = vaccinations;
        } catch (IOException e) {
            System.out.println(e);
        }
    }

    //Returns orders and vaccines amounts in given time period
    public TotalResponse getTotalOrderAndVaccinesAmount(String since, String until) {
            TotalResponse totalResponse = new TotalResponse();
            vaccineOrdersAndAmounts(this.solarBuddhica, since, until, totalResponse);
            vaccineOrdersAndAmounts(this.antiqua, since, until, totalResponse);
            vaccineOrdersAndAmounts(this.zerpfy, since, until, totalResponse);
            return totalResponse;
    }
    //Loops through the producers lists and adds orders and vaccines to totalResponse
    public void vaccineOrdersAndAmounts(List<Vaccine> producers, String since, String until, TotalResponse totalResponse){
        sinceDate = parseTime(since);
        untilDate = parseTime(until);
        for (Vaccine vaccine : producers) {
            arrivedDate = parseTime(vaccine.arrived);
            if (sinceDate.before(arrivedDate) && untilDate.after(arrivedDate)) {
                totalResponse.vaccines += vaccine.injections;
                totalResponse.orders++;
            }
        }
    }
    //Gets total amount of all time from all producers
    public TotalResponse getTotalAmount(){
        TotalResponse totalResponse = new TotalResponse();
        for (Vaccine vaccine : this.solarBuddhica) {
            totalResponse.vaccines += vaccine.injections;
            totalResponse.orders++;

        }
        for (Vaccine vaccine : this.zerpfy) {
            totalResponse.vaccines += vaccine.injections;
            totalResponse.orders++;
        }
        for (Vaccine vaccine : this.antiqua) {
            totalResponse.vaccines += vaccine.injections;
            totalResponse.orders++;

        }
        return totalResponse;
    }
    //Checks how many vaccinations have been done in given time period
    public VaccinationResponse getTotalVaccinationsDone(String since, String until) {
            sinceDate = parseTime(since);
            untilDate = parseTime(until);
            VaccinationResponse vaccinationResponse = new VaccinationResponse();
            for (Vaccination vaccination : this.vaccinations) {
                    dateVaccinated = parseTime(vaccination.vaccinationDate);
                    if (sinceDate.before(dateVaccinated) && untilDate.after(dateVaccinated)) {
                        vaccinationResponse.vaccinationsDone++;
                    }
            }
            return vaccinationResponse;
    }
    //Gets amount of orders and injections from SolarBuddhica producer
    public TotalResponse getTotalSolarBuddhica(String since, String until) {
        TotalResponse totalResponse = new TotalResponse();
        vaccineOrdersAndAmounts(this.solarBuddhica, since, until, totalResponse);
        return totalResponse;
    }
    //Gets amount of orders and injections from Antiqua producer
    public TotalResponse getTotalAntiqua(String since, String until) {
        TotalResponse totalResponse = new TotalResponse();
        vaccineOrdersAndAmounts(this.antiqua, since, until, totalResponse);
        return totalResponse;

    }
    //Gets amount of orders and injections from Zerpfy producer
    public TotalResponse getTotalZerpfy(String since, String until) {
        TotalResponse totalResponse = new TotalResponse();
        vaccineOrdersAndAmounts(this.zerpfy, since, until, totalResponse);
        return totalResponse;
    }
    //Keeps count from total expired vaccines that are 30days older than given time
    public VaccineExpired getTotalExpired(String until){
        VaccineExpired vaccineExpired = new VaccineExpired();
        totalVaccineExpired(this.solarBuddhica, until, vaccineExpired);
        totalVaccineExpired(this.antiqua, until, vaccineExpired);
        totalVaccineExpired(this.zerpfy, until, vaccineExpired);
        return vaccineExpired;
    }
    //Adds older vaccines older than 30days to vaccineExpired
    public void totalVaccineExpired(List<Vaccine> producers, String until, VaccineExpired expired){
        untilDate = substract30Days(until);
        for (Vaccine vaccine : producers) {
            arrivedDate = parseTime(vaccine.arrived);
            if (untilDate.after(arrivedDate)) {
                expired.vaccineExpired += vaccine.injections;
            }
        }
    }
    //Keeps count how many vaccines has expired before usage until given time
    public VaccineExpiredBeforeUsage getTotalExpiredBeforeUsage(String until){
        VaccineExpiredBeforeUsage vaccineExpiredBeforeUsage = new VaccineExpiredBeforeUsage();
        vaccinesExpiredBefore(this.solarBuddhica, until, vaccineExpiredBeforeUsage);
        vaccinesExpiredBefore(this.antiqua, until, vaccineExpiredBeforeUsage);
        vaccinesExpiredBefore(this.zerpfy, until, vaccineExpiredBeforeUsage);

        return vaccineExpiredBeforeUsage;
    }
    //Goes through the producers vaccines and if vaccine is expired count increases and if vaccine has been uses count decreases
    public void vaccinesExpiredBefore(List<Vaccine> producers, String until, VaccineExpiredBeforeUsage vaccineExpiredBeforeUsage){
        untilDate = substract30Days(until);
        for (Vaccine vaccine : producers) {
            arrivedDate = parseTime(vaccine.arrived);
            if (untilDate.after(arrivedDate)) {
                vaccineExpiredBeforeUsage.expiredBeforeUsage += vaccine.injections;
                vaccineExpiredBeforeUsage.expiredBeforeUsage -= vaccinationGiven(vaccine);
            }
        }
    }
    //Checks how many vaccines are left. Adds total injections received and deletes used injections
    public VaccinesLeft getTotalVaccinesLeft(){
        VaccinesLeft vaccinesLeft = new VaccinesLeft();
        for(Vaccine vaccine : this.solarBuddhica){
            vaccinesLeft.vaccinesLeft += vaccine.injections;
        }
        for(Vaccine vaccine : this.zerpfy){
            vaccinesLeft.vaccinesLeft += vaccine.injections;
        }
        for(Vaccine vaccine : this.antiqua){
            vaccinesLeft.vaccinesLeft += vaccine.injections;
        }
        vaccinesLeft.vaccinesLeft -= vaccinations.size();
        return vaccinesLeft;
    }
    //Returns total vaccines left which have arrived in 30days and not used
    public VaccinesLeft getTotalVaccinesLeftNotExpired(String since){
        VaccinesLeft vaccinesLeft = new VaccinesLeft();
        vaccinesLeftNotExpired(this.solarBuddhica, since, vaccinesLeft);
        vaccinesLeftNotExpired(this.antiqua, since, vaccinesLeft);
        vaccinesLeftNotExpired(this.zerpfy, since, vaccinesLeft);
        return vaccinesLeft;
    }
    //If vaccine is less than 30days old vaccinesLeft increases and if vaccination has been given it decreases
    public void vaccinesLeftNotExpired(List<Vaccine> producers, String since, VaccinesLeft vaccinesLeft){
        untilDate = parseTime(since);
        sinceDate = substract30Days(since);
        for (Vaccine vaccine : producers) {
            arrivedDate = parseTime(vaccine.arrived);
            if (sinceDate.before(arrivedDate) && untilDate.after(arrivedDate)) {
                vaccinesLeft.vaccinesLeft += vaccine.injections;
                vaccinesLeft.vaccinesLeft -= vaccinationGiven(vaccine);
            }
        }
    }
    //Checks how many vaccinations has been given from bottle
    public int vaccinationGiven(Vaccine vaccine){
        int vaccinesGivenBeforeExpire = 0;
        for(Vaccination vaccination : this.vaccinations){
            if(vaccination.sourceBottle.equals(vaccine.id)){
                vaccinesGivenBeforeExpire++;
            }
        } return vaccinesGivenBeforeExpire;
    }
    //Parses string to given date format
    public Date parseTime(String dateStr){
        try{
            Date date = df1.parse(dateStr);
            return date;
        } catch (ParseException e) {
            System.out.println("Error in parseTime function");
            System.out.println(e);
        } return null;
    }
    //Decreases 30 days from given time and parses to right time format
    public Date substract30Days(String dateStr){
        try{
            Calendar c = Calendar.getInstance();
            c.setTime(df1.parse(dateStr));
            c.add(Calendar.DATE, -30);
            Date date = c.getTime();
            return date;
        } catch (ParseException e) {
            System.out.println("Error in expiredDate function");
            System.out.println(e);
        } return null;
    }
}
