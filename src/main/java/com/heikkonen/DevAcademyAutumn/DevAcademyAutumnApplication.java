package com.heikkonen.DevAcademyAutumn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DevAcademyAutumnApplication {

	public static void main(String[] args) {

		SpringApplication.run(DevAcademyAutumnApplication.class, args);
	}

}
