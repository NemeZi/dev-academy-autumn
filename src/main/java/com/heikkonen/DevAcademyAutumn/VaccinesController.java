package com.heikkonen.DevAcademyAutumn;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vaccines")
public class VaccinesController {

    private VaccinesService vaccinesService;

    public VaccinesController(VaccinesService vaccinesService) {
        this.vaccinesService = vaccinesService;

    }
    //Shows total vaccine amounts and orders in given time period
    @GetMapping("/total")
    public TotalResponse getTotalOrderAndVaccinesAmount(@RequestParam String since, @RequestParam String until) {
        return vaccinesService.getTotalOrderAndVaccinesAmount(since, until);
    }
    //Shows total vaccine and order amounts in all time
    @GetMapping("/totalamount")
    public TotalResponse getTotalAmount(){
        return vaccinesService.getTotalAmount();
    }
    //Shows how many vaccines have been used in given time period
    @GetMapping("/used")
    public VaccinationResponse getTotalVaccinationsDone(@RequestParam String since, @RequestParam String until) {
        return vaccinesService.getTotalVaccinationsDone(since, until);
    }
    //Shows total amount of vaccines and orders from SolarBuddhica in give time period
    @GetMapping("/solarbuddhica")
    public TotalResponse getTotalSolarBuddhica(@RequestParam String since, @RequestParam String until) {
        return vaccinesService.getTotalSolarBuddhica(since, until);
    }
    //Shows total amount of vaccines and orders from Antiqua in give time period
    @GetMapping("/antiqua")
    public TotalResponse getTotalAntiqua(@RequestParam String since, @RequestParam String until) {
        return vaccinesService.getTotalAntiqua(since, until);
    }
    //Shows total amount of vaccines and orders from Zerpfy in give time period
    @GetMapping("/zerpfy")
    public TotalResponse getTotalZerpfy(@RequestParam String since, @RequestParam String until) {
        return vaccinesService.getTotalZerpfy(since, until);
    }
    //Shows expired vaccines of all time including injected ones
    @GetMapping("/expired")
    public VaccineExpired getTotalExpired(@RequestParam String until) {
        return vaccinesService.getTotalExpired(until);
    }
    //Shows expired vaccines before usage before given time
    @GetMapping("/expiredbeforeusage")
    public VaccineExpiredBeforeUsage getTotalExpiredBeforeUsage(@RequestParam String until){
        return vaccinesService.getTotalExpiredBeforeUsage(until);
    }
    //Shows how many vaccines are left in total
    @GetMapping("/vaccinesleft")
    public VaccinesLeft getTotalVaccinesLeft(){
        return vaccinesService.getTotalVaccinesLeft();
    }
    //Shows how many vaccines are left and not expired
    @GetMapping("/notexpiredleft")
    public VaccinesLeft getTotalVaccinesLeftNotExpired(@RequestParam String since){
        return vaccinesService.getTotalVaccinesLeftNotExpired(since);
    }
}
