package com.heikkonen.DevAcademyAutumn;

import lombok.Data;

@Data
public class Vaccine {

    public String id;
    public int orderNumber;
    public String responsiblePerson;
    public String healthCareDistrict;
    public String vaccine;
    public int injections;
    public String arrived;

    Vaccine(){}

    public Vaccine(String id, int orderNumber, String responsiblePerson, String healthCareDistrict, String vaccine, int injections, String arrived) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.responsiblePerson = responsiblePerson;
        this.healthCareDistrict = healthCareDistrict;
        this.vaccine = vaccine;
        this.injections = injections;
        this.arrived = arrived;
    }
}
