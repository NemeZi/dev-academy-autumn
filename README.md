# Instruction for Java API made with Spring Boot.

## How to run:
1. Download repository
2. Run DevAcademyAutumnApplication in src/main/java/com.heikkonen.DevAcademyAutumn

## How to run tests:
Run DevAcademyAutumnApplicationTests in src/test/java/com.heikkonen.DevAcademyAutumn

### Get total amount of vaccines and orders
`GET localhost:8080/api/vaccines/totalamount`

### Get total vaccines and orders in given time period
`GET localhost:8080/api/vaccines/total?since=yyyy-mm-ddThh:mm&until=yyyy-mm-ddThh:mm`

### Get used vaccines in given time period
`GET localhost:8080/api/vaccines/used?since=yyyy-mm-ddThh:mm&until=yyyy-mm-ddThh:mm`
<br>Shows used vaccines between given time period

### Get orders and vaccine amounts in given time period from SolarBuddhica
`GET localhost:8080/api/vaccines/solarbuddhica?since=yyyy-mm-ddThh:mm&until=yyyy-mm-ddThh:mm`
<br>Shows order and vaccine amount from solarbuddhica producer

### Get orders and vaccine amounts in given time period from SolarBuddhica
`GET localhost:8080/api/vaccines/antiqua?since=yyyy-mm-ddThh:mm&until=yyyy-mm-ddThh:mm`
<br>Shows order and vaccine amount from antiqua producer

### Get orders and vaccine amounts in given time period from SolarBuddhica
`GET localhost:8080/api/vaccines/zerpfy?since=yyyy-mm-ddThh:mm&until=yyyy-mm-ddThh:mm`
<br>Shows order and vaccine amount from Zerpfy producer

### Get expired vaccines till given time
`GET localhost:8080/api/vaccines/expired?until=yyyy-mm-ddThh:mm`
(Shows all expired vaccines until given time)

### Get expired vaccines before usage in given time period
`GET localhost:8080/api/vaccines/expiredbeforeusage?until=yyyy-mm-ddThh:mm`
<br>Shows expired vaccines before usage until given time period

### Get total vaccines not used of all time
`GET localhost:8080/api/vaccines/vaccinesleft`
<br>Shows how many vaccines are not yet used from orders of all time

### Get total vaccines which are not used or expired till given date
`GET localhost:8080/api/vaccines/notexpiredleft?since=yyyy-mm-ddThh:mm`
<br>Shows how many vaccines are not yet used and not yet expired in the given date


_Made by: Tomi Heikkonen_
